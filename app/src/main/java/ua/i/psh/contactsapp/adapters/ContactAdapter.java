package ua.i.psh.contactsapp.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.i.psh.contactsapp.R;
import ua.i.psh.contactsapp.helpers.CircleTransform;
import ua.i.psh.contactsapp.models.ContactBook;

/**
 * Created by Serhii Polishchuk p.s.h@i.ua on 30-Aug-17.
 */

public class ContactAdapter extends BaseAdapter {

    private Context ctx;
    private List<ContactBook> lists;

    public ContactAdapter (Context ctx, List<ContactBook> lists){
        this.ctx = ctx;
        this.lists = lists;
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int i) {
        return lists.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view == null){
            view = ((LayoutInflater)this.ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
                    inflate(R.layout.i_contact_field, null, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }else{
            holder = (ViewHolder) view.getTag();
        }

        String itemName = this.lists.get(i).getUserFirstName();
        holder.userName.setText(itemName);
//        holder.userPhone.setText(this.lists.get(i).getUserMobilePhone());
        StringBuilder info = new StringBuilder();
        if(this.lists.get(i).getUserBasePhoto() == null){
            info.append(String.valueOf(itemName.
                    charAt(0)).
                    toUpperCase());
            boolean run = true;
            int position = 1;
            while (run){
                if(itemName.charAt(position) == ' '){
                    position ++;
                    info.append(String.valueOf(itemName.
                            charAt(position)).
                            toUpperCase());
                    run = false;
                }
                position++;
                if(position == itemName.length()) run = false;
            }
            holder.userPhotoAlt.setText(info.toString());
            holder.userPhotoAlt.setTextColor(Color.argb(255,
                    new Random().nextInt(255),
                    new Random().nextInt(255),
                    new Random().nextInt(255)));
            showPhoto(holder, false);
        }else{
            Picasso.with(this.ctx).
                    load(this.lists.get(i).getUserBasePhoto()).
                    transform(new CircleTransform()).
                    fit().
                    into(holder.userPhoto);
            showPhoto(holder, true);
        }

        return view;
    }

    private void showPhoto(ViewHolder holder, boolean isShow){
        if(isShow){
            holder.userPhoto.setVisibility(View.VISIBLE);
            holder.userPhotoAltContainer.setVisibility(View.GONE);
        }else{
            holder.userPhoto.setVisibility(View.GONE);
            holder.userPhotoAltContainer.setVisibility(View.VISIBLE);
        }
    }

    public void updateListContacts(List<ContactBook>contactlist){
        this.lists = contactlist;
        notifyDataSetChanged();
    }

    protected class ViewHolder{

        @BindView(R.id.userPhoto)
        public ImageView userPhoto;

        @BindView(R.id.userPhotoAlt)
        public TextView userPhotoAlt;

        @BindView(R.id.userName)
        public TextView userName;

//        @BindView(R.id.userPhone)
//        public TextView userPhone;

        @BindView(R.id.containerPhotoAlt)
        public ConstraintLayout userPhotoAltContainer;

        ViewHolder (View view){
            ButterKnife.bind(this, view);
        }
    }

}
