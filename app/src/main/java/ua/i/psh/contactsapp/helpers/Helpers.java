package ua.i.psh.contactsapp.helpers;

import android.graphics.Color;

import java.util.Random;

/**
 * Created by Serhii Polishchuk p.s.h@i.ua on 31-Aug-17.
 */

public class Helpers {

    public static String getUserNameResult(String userName) {
        StringBuilder info = new StringBuilder();
        info.append(String.valueOf(userName.
                charAt(0)).
                toUpperCase());
        boolean run = true;
        int position = 1;
        while (run) {
            if (userName.charAt(position) == ' ') {
                position++;
                info.append(String.valueOf(userName.
                        charAt(position)).
                        toUpperCase());
                run = false;
            }
            position++;
            if (position == userName.length()) run = false;
        }
        return info.toString();
    }
}
