package ua.i.psh.contactsapp.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.i.psh.contactsapp.R;
import ua.i.psh.contactsapp.activities.Main;
import ua.i.psh.contactsapp.helpers.Helpers;
import ua.i.psh.contactsapp.models.ContactBook;

/**
 * Created by Serhii Polishchuk p.s.h@i.ua on 31-Aug-17.
 */

public class DetailContact extends Fragment {

    @BindView(R.id.mainToolbar)
    protected Toolbar toolbar;

    @BindView(R.id.mainPhotoUser)
    protected ImageView userPhoto;

    @BindView(R.id.mainPhotoUserAlt)
    protected TextView userPhotoAlt;

    @BindView(R.id.userNameFieldVal)
    protected TextView userNameVal;

    @BindView(R.id.userPhoneFieldVal)
    protected TextView userPhoneVal;

    @BindView(R.id.userMailFieldVal)
    protected TextView userMailVal;

    @BindView(R.id.userAnotherFieldVal)
    protected TextView userAnotherInfo;

    @BindView(R.id.containerUserPhoto)
    protected FrameLayout container;

    private ContactBook contact = null;

    private WeakReference<Main> activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_detail_contact, null, false);
        ButterKnife.bind(this, v);
        activity = new WeakReference<Main>((Main)getActivity());
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        contact = activity.get().getContact();
        if(contact!=null) {
            toolbar.setTitle(contact.getUserFirstName());
            if(contact.getUserFirstName()!=null) userNameVal.setText(contact.getUserFirstName());
            if(contact.getUserMobilePhone()!=null) userPhoneVal.setText(contact.getUserMobilePhone());
            if(contact.getMail()!=null) userMailVal.setText(contact.getMail());
            if(contact.getUserBasePhoto() == null){
                switchPhotoView(false);
                userPhotoAlt.setText(Helpers.getUserNameResult(contact.getUserFirstName()));
            }else {
                switchPhotoView(true);
                Picasso.with(getContext()).
                        load(contact.getUserBasePhoto()).
                        into(userPhoto);
            }
        }
        activity.get().setSupportActionBar(toolbar);
    }

    private void switchPhotoView(boolean isView){
        if(isView) {
            userPhoto.setVisibility(View.VISIBLE);
            userPhotoAlt.setVisibility(View.GONE);
        }else{
            userPhoto.setVisibility(View.GONE);
            userPhotoAlt.setVisibility(View.VISIBLE);
            container.setBackgroundColor(Color.argb(255,
                    new Random().nextInt(255),
                    new Random().nextInt(255),
                    new Random().nextInt(255)));
        }
    }
}
