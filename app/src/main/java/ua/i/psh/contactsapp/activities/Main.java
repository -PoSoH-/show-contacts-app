package ua.i.psh.contactsapp.activities;

import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.i.psh.contactsapp.R;
import ua.i.psh.contactsapp.fragments.DetailContact;
import ua.i.psh.contactsapp.fragments.ListContacts;
import ua.i.psh.contactsapp.models.ContactBook;

public class Main extends AppCompatActivity {

    @BindView(R.id.progressBarMain)
    protected ProgressBar progressView;

    private int container = R.id.containerForFragment;
    private ContactsContract.Contacts contacts;
    private List<ContactBook> contactlist = new ArrayList<>();

    public static final  String LIST = "fill.list" ;
    public static final  String DETAILS = "detail.contact" ;

    private int positionSelected = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_main);
        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        enableProgressView();
        readContacts();
        loadListFragment();
        disableProgressView();
        Fragment f = getSupportFragmentManager().findFragmentById(container);
        if(f==null) setFragmentWithFrontAnimation(getSupportFragmentManager(), new ListContacts(), container, false);
        else {
            if(f instanceof ListContacts)
                ((ListContacts)f).updateList(contactlist);
        }
    }

    private void readContacts(){
        if(contactlist != null) contactlist = new ArrayList<>();
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        assert phones != null;
        while (phones.moveToNext())
        {
            ContactBook contact = new ContactBook();
            contact.setUserFirstName(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
            contact.setUserSecondName(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_ALTERNATIVE)));
            contact.setUserMobilePhone(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
            contact.setUserBasePhoto(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI)));
            contact.setContactId(phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID)));
            contact.setMail(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.SEND_TO_VOICEMAIL)));
            contactlist.add(contact);
        }
        phones.close();
    }

    private void enableProgressView(){progressView.setVisibility(View.VISIBLE);}
    private void disableProgressView(){progressView.setVisibility(View.GONE);}
    private void loadListFragment(){

    }

    public List<ContactBook> getLists() {return contactlist;}
    public ContactBook getContact() {
        if(positionSelected >= 0)
            return contactlist.get(positionSelected);
        return null;
    }

    public void setSelectedContact(int positionContact){
        Log.e("RES", "Contact position selected " + positionContact );
        this.positionSelected = positionContact;
        setFragmentWithFrontAnimation(getSupportFragmentManager(),
                new DetailContact(),
                container,
                false);
    }

    public static void setFragmentWithFrontAnimation(FragmentManager fragmentManager, Fragment fragment,
                                                    int layoutResIs, boolean addToBackStack){
        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(
                R.anim.slide_left_0,
                R.anim.slide_left_1,
                R.anim.slide_right_0,
                R.anim.slide_right_1);
        fragmentTransaction.replace(layoutResIs, fragment, tag);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public static void setFragmentWithBackAnimation(FragmentManager fragmentManager, Fragment fragment,
                                                    int layoutResIs, boolean addToBackStack){
        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(
                R.anim.slide_right_0,
                R.anim.slide_right_1,
                R.anim.slide_left_0,
                R.anim.slide_left_1);
        fragmentTransaction.replace(layoutResIs, fragment, tag);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(container);
        if(f != null && f instanceof DetailContact){
            setFragmentWithBackAnimation(getSupportFragmentManager(), new ListContacts(), container, false);
            return;
        }
        super.onBackPressed();
    }
}
