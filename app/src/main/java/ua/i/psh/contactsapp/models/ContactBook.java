package ua.i.psh.contactsapp.models;

import android.provider.ContactsContract;

/**
 * Created by Serhii Polishchuk p.s.h@i.ua on 30-Aug-17.
 */

public class ContactBook {

    int contactId = 0;

    String userFirstName = null;
    String userSecondName = null;
    String userMobilePhone = null;
    String userBasePhoto = null;

    String mail = null;
    Long addressIndex = null;
    String AddressCount = null;
    String AddressTitles = null;

    public int getContactId() {
        return contactId;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Long getAddressIndex() {
        return addressIndex;
    }

    public void setAddressIndex(Long addressIndex) {
        this.addressIndex = addressIndex;
    }

    public String getAddressCount() {
        return AddressCount;
    }

    public void setAddressCount(String addressCount) {
        AddressCount = addressCount;
    }

    public String getAddressTitles() {
        return AddressTitles;
    }

    public void setAddressTitles(String addressTitles) {
        AddressTitles = addressTitles;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public void setUserSecondName(String userSecondName) {
        this.userSecondName = userSecondName;
    }

    public void setUserMobilePhone(String userMobilePhone) {
        this.userMobilePhone = userMobilePhone;
    }

    public void setUserBasePhoto(String userBasePhoto) {
        this.userBasePhoto = userBasePhoto;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public String getUserSecondName() {
        return userSecondName;
    }

    public String getUserMobilePhone() {
        return userMobilePhone;
    }

    public String getUserBasePhoto() {
        return userBasePhoto;
    }
}
