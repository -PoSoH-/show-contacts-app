package ua.i.psh.contactsapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ua.i.psh.contactsapp.R;

/**
 * Created by Serhii Polishchuk p.s.h@i.ua on 31-Aug-17.
 */

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        setContentView(R.layout.a_splash);
        setTheme(R.style.AppThemeFull);
        super.onCreate(savedInstanceState);

        Intent i = new Intent(this, Main.class);
        this.startActivity(i);
        this.finish();
    }
}
