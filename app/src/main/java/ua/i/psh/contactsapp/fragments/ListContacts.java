package ua.i.psh.contactsapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import butterknife.OnItemSelected;
import ua.i.psh.contactsapp.R;
import ua.i.psh.contactsapp.activities.Main;
import ua.i.psh.contactsapp.adapters.ContactAdapter;
import ua.i.psh.contactsapp.models.ContactBook;

/**
 * Created by Serhii Polishchuk p.s.h@i.ua on 30-Aug-17.
 */

public class ListContacts extends Fragment {

    @BindView(R.id.listContactsInfo)
    protected ListView listContacts;

    @BindView(R.id.listToolbar)
    protected Toolbar toolbar;

    @OnItemClick(R.id.listContactsInfo)
    protected void onSelectedContact(int position){
        ((Main)getActivity()).setSelectedContact(position);
    }

    private ContactAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_list_contacts, null, false); /*super.onCreateView(inflater, container, savedInstanceState);*/
        ButterKnife.bind(this, v);
        adapter = new ContactAdapter(getContext(), ((Main)getActivity()).getLists());
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listContacts.setAdapter(adapter);
        toolbar.setTitle("All Contacts");
        ((Main)getActivity()).setSupportActionBar(toolbar);
    }

    public void updateList(List<ContactBook>contactlist){
        adapter.updateListContacts(contactlist);
    }
}
